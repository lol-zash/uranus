import { TypeOrmModuleOptions } from '@nestjs/typeorm';

function ormConfig(): TypeOrmModuleOptions {
    const commonConf = {
        SYNCRONIZE: false,
        ENTITIES: [__dirname + '/domain/*.entity{.ts,.js}'],
        MIGRATIONS: [__dirname + '/migrations/**/*{.ts,.js}'],
        CLI: {
            migrationsDir: 'src/migrations',
        },
        MIGRATIONS_RUN: true,
    };

    let ormconfig: TypeOrmModuleOptions = {
        name: 'default',
        type: 'sqlite',
        database: '../target/db/sqlite-dev-db.sql',
        logging: true,
        synchronize: true,
        entities: commonConf.ENTITIES,
        migrations: commonConf.MIGRATIONS,
        cli: commonConf.CLI,
        migrationsRun: commonConf.MIGRATIONS_RUN,
    };

    if (process.env.BACKEND_ENV === 'prod') {
        ormconfig = {
            name: 'default',
            type: 'postgres',
            schema: 'production',
            database: 'uranus',
            host: 'localhost',
            port: 5433,
            username: 'sa',
            password: 'yourStrong(!)Password',
            logging: false,
            synchronize: commonConf.SYNCRONIZE,
            entities: commonConf.ENTITIES,
            migrations: commonConf.MIGRATIONS,
            cli: commonConf.CLI,
            migrationsRun: false,
        };
    }

    if (process.env.BACKEND_ENV === 'dev') {
        ormconfig = {
          name: 'default',
          type: 'postgres',
          schema: 'development',
          database: 'uranus',
          host: 'uranus-dev.cvwurbtvq3tb.eu-central-1.rds.amazonaws.com',
          port: 5432,
          username: 'zash_uranus',
          password: 'liquiddrumandbassisthebest!123',
          logging: false,
          synchronize: commonConf.SYNCRONIZE,
          entities: commonConf.ENTITIES,
          migrations: commonConf.MIGRATIONS,
          cli: commonConf.CLI,
          migrationsRun: true,
      };
    }
    return ormconfig;
}

export { ormConfig };
